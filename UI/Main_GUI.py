import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import Menu

win = tk.Tk()

win.title("Main GUI")

mighty = ttk.LabelFrame(win, text="Mighty Python")
mighty.grid(column=0, row=0, padx=8, pady=4)
a_label = ttk.Label(mighty, text="Enter your name")
a_label.grid(column=0, row=0, sticky="W")


def click_me():
    action.configure(text="Hello " + name.get() + " " + number.get())


action = ttk.Button(mighty, text="Click", command=click_me)
action.grid(column=2, row=1)
name = tk.StringVar()
name_entered = ttk.Entry(mighty, width=12, textvariable=name)
name_entered.grid(column=0, row=1, sticky=tk.W)

ttk.Label(mighty, text="Choose a number:").grid(column=1, row=0)
number = tk.StringVar()
number_chosen = ttk.Combobox(mighty, width=12, textvariable=number, state='readonly')
number_chosen['values'] = (1, 2, 4, 42, 100)
number_chosen.grid(column=1, row=1)
number_chosen.current(0)
name_entered.focus()

chVarDis = tk.IntVar()
check1 = tk.Checkbutton(mighty, text="Disabled", variable=chVarDis, state='disabled')
check1.select()
check1.grid(column=0, row=4, sticky=tk.W)

chVarUn = tk.IntVar()
check2 = tk.Checkbutton(mighty, text="UnChecked", variable=chVarUn)
check2.deselect()
check2.grid(column=1, row=4, sticky=tk.W)

chVarEn = tk.IntVar()
check3 = tk.Checkbutton(mighty, text="Checked", variable=chVarEn)
check3.select()
check3.grid(column=2, row=4, sticky=tk.W)


def radCall():
    radSel = radVar.get()
    if radSel == 0: win.configure(background=colors[0])
    elif radSel == 1: win.configure(background=colors[1])
    elif radSel == 2: win.configure(background=colors[2])


def _quit():
    win.quit()
    win.destroy()
    exit()


colors = ["Blue", "Gold", "Red"]

radVar = tk.IntVar()
radVar.set(99)

for col in range(3):
    rad = tk.Radiobutton(mighty, text=colors[col], variable=radVar, value=col, command=radCall)
    rad.grid(column=col, row=5, sticky=tk.W, columnspan=3)

scroll_w = 30
scroll_h = 3

scr = scrolledtext.ScrolledText(mighty, width=scroll_w, height=scroll_h, wrap=tk.WORD)
scr.grid(column=0, columnspan=3, sticky=tk.EW)

buttons_frame = ttk.LabelFrame(mighty, text='Labels in frame')
buttons_frame.grid(column=0, row=7)

ttk.Label(buttons_frame,text="Label 1").grid(column=0, row=0, sticky=tk.W)
ttk.Label(buttons_frame,text="Label 2").grid(column=1, row=0, sticky=tk.W)
ttk.Label(buttons_frame,text="Label 3").grid(column=2, row=0, sticky=tk.W)

for child in buttons_frame.winfo_children():
    child.grid_configure(padx=8, pady=4)

menu_bar = Menu(win)
win.config(menu=menu_bar)

file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="New")
file_menu.add_separator()
file_menu.add_command(label="Exit", command=_quit)

help_menu = Menu(menu_bar, tearoff=0)
help_menu.add_command(label="About")

menu_bar.add_cascade(label="File", menu=file_menu)
menu_bar.add_cascade(label="Help", menu=help_menu)

win.mainloop()
