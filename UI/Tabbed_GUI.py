import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import Menu
from tkinter import Spinbox
from tkinter import messagebox as msg

win = tk.Tk()
win.title("Tabbed GUI")

tabControl = ttk.Notebook(win)
tab1 = ttk.Frame(tabControl)
tabControl.add(tab1, text="Tab 1")

tab2 = ttk.Frame(tabControl)
tabControl.add(tab2, text="Tab 2")

tabControl.pack(expand=1, fill="both")

mighty = ttk.LabelFrame(tab1, text="Mighty Python ")
mighty.grid(column=0, row=0, padx=8, pady=4)

a_label = ttk.Label(mighty, text="Enter a name: ")
a_label.grid(column=0, row=0, sticky=tk.W)

b_label = ttk.Label(mighty, text="Choose a number: ")
b_label.grid(column=1, row=0, sticky=tk.W)

mighty2 = ttk.LabelFrame(tab2, text="Snake")
mighty2.grid(column=0, row=0, padx=8, pady=4)


def click_me():
    action.configure(text="Hello " + name.get() + " " + number.get())


action = ttk.Button(mighty, text="Click", command=click_me)
action.grid(column=2, row=1)
name = tk.StringVar()
name_entered = ttk.Entry(mighty, width=12, textvariable=name)
name_entered.grid(column=0, row=1, sticky=tk.W)

ttk.Label(mighty, text="Choose a number:").grid(column=1, row=0)
number = tk.StringVar()
number_chosen = ttk.Combobox(mighty, width=12, textvariable=number, state='readonly')
number_chosen['values'] = (1, 2, 4, 42, 100)
number_chosen.grid(column=1, row=1)
number_chosen.current(0)
name_entered.focus()

chVarDis = tk.IntVar()
check1 = tk.Checkbutton(mighty2, text="Disabled", variable=chVarDis, state='disabled')
check1.select()
check1.grid(column=0, row=4, sticky=tk.W)

chVarUn = tk.IntVar()
check2 = tk.Checkbutton(mighty2, text="UnChecked", variable=chVarUn)
check2.deselect()
check2.grid(column=1, row=4, sticky=tk.W)

chVarEn = tk.IntVar()
check3 = tk.Checkbutton(mighty2, text="Checked", variable=chVarEn)
check3.select()
check3.grid(column=2, row=4, sticky=tk.W)


def radCall():
    radSel = radVar.get()
    if radSel == 0: mighty2.configure(text=colors[0])
    elif radSel == 1: mighty2.configure(text=colors[1])
    elif radSel == 2: mighty2.configure(text=colors[2])


def _quit():
    win.quit()
    win.destroy()
    exit()


colors = ["Blue", "Gold", "Red"]

radVar = tk.IntVar()
radVar.set(99)

for col in range(3):
    rad = tk.Radiobutton(mighty2, text=colors[col], variable=radVar, value=col, command=radCall)
    rad.grid(column=col, row=5, sticky=tk.W, columnspan=3)

scroll_w = 30
scroll_h = 3

scroll = tk.StringVar()
scr = scrolledtext.ScrolledText(mighty, width=scroll_w, height=scroll_h, wrap=tk.WORD)
scr.grid(column=0, row=3, columnspan=3, sticky=tk.EW)

buttons_frame = ttk.LabelFrame(mighty2, text='Labels in frame')
buttons_frame.grid(column=0, row=7)

ttk.Label(buttons_frame,text="Label 1").grid(column=0, row=0, sticky=tk.W)
ttk.Label(buttons_frame,text="Label 2").grid(column=1, row=0, sticky=tk.W)
ttk.Label(buttons_frame,text="Label 3").grid(column=2, row=0, sticky=tk.W)

menu_bar = Menu(win)
win.config(menu=menu_bar)

file_menu = Menu(menu_bar, tearoff=0)
file_menu.add_command(label="New")
file_menu.add_separator()
file_menu.add_command(label="Exit", command=_quit)


def _msg_box():
    msg.showwarning("Message box info", "Text in message box info")


help_menu = Menu(menu_bar, tearoff=0)
help_menu.add_command(label="About", command=_msg_box)

menu_bar.add_cascade(label="File", menu=file_menu)
menu_bar.add_cascade(label="Help", menu=help_menu)


def _spin():
    value = spin.get()
    print(value)
    scr.insert(tk.INSERT, value + '\n')


spin = Spinbox(mighty, values=(1, 2, 4, 42, 100), from_=0, to=10, width=5, command=_spin)
spin.grid(column=0, row=2)

win.mainloop()
